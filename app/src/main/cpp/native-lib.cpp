#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_pl_jesmanczyk_android_driverassistant_ImageUtils_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Using native speed-up";
    return env->NewStringUTF(hello.c_str());
}

#ifndef MAX
#define MAX(a, b) ({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a > _b ? _a : _b; })
#define MIN(a, b) ({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a < _b ? _a : _b; })
#endif

// This value is 2 ^ 18 - 1, and is used to clamp the RGB values before their ranges
// are normalized to eight bits.
static const int kMaxChannelValue = 262143;

static inline uint32_t YUV2RGB(int nY, int nU, int nV) {
    nY -= 16;
    nU -= 128;
    nV -= 128;
    if (nY < 0) nY = 0;

    // This is the floating point equivalent. We do the conversion in integer
    // because some Android devices do not have floating point in hardware.
    // nR = (int)(1.164 * nY + 2.018 * nU);
    // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
    // nB = (int)(1.164 * nY + 1.596 * nV);

    int nR = 1192 * nY + 1634 * nV;
    int nG = 1192 * nY - 833 * nV - 400 * nU;
    int nB = 1192 * nY + 2066 * nU;

    nR = MIN(kMaxChannelValue, MAX(0, nR));
    nG = MIN(kMaxChannelValue, MAX(0, nG));
    nB = MIN(kMaxChannelValue, MAX(0, nB));

    nR = (nR >> 10) & 0xff;
    nG = (nG >> 10) & 0xff;
    nB = (nB >> 10) & 0xff;

    return 0xff000000 | (nR << 16) | (nG << 8) | nB;
}

//  Accepts a YUV 4:2:0 image with a plane of 8 bit Y samples followed by an
//  interleaved U/V plane containing 8 bit 2x2 subsampled chroma samples,
//  except the interleave order of U and V is reversed. Converts to a packed
//  ARGB 32 bit output of the same pixel dimensions.
void ConvertYUV420SPToARGB8888(const uint8_t* const yData,
                               const uint8_t* const uvData,
                               uint32_t* const output, const int width,
                               const int height) {
    const uint8_t* pY = yData;
    const uint8_t* pUV = uvData;
    uint32_t* out = output;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int nY = *pY++;
            int offset = (y >> 1) * width + 2 * (x >> 1);
#ifdef __APPLE__
            int nU = pUV[offset];
      int nV = pUV[offset + 1];
#else
            int nV = pUV[offset];
            int nU = pUV[offset + 1];
#endif

            *out++ = YUV2RGB(nY, nU, nV);
        }
    }
}

// The same as above, but downsamples each dimension to half size.
void ConvertYUV420SPToARGB8888HalfSize(const uint8_t* const input,
                                       uint32_t* const output, int width,
                                       int height) {
    const uint8_t* pY = input;
    const uint8_t* pUV = input + (width * height);
    uint32_t* out = output;
    int stride = width;
    width >>= 1;
    height >>= 1;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int nY = (pY[0] + pY[1] + pY[stride] + pY[stride + 1]) >> 2;
            pY += 2;
#ifdef __APPLE__
            int nU = *pUV++;
      int nV = *pUV++;
#else
            int nV = *pUV++;
            int nU = *pUV++;
#endif

            *out++ = YUV2RGB(nY, nU, nV);
        }
        pY += stride;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_pl_jesmanczyk_android_driverassistant_ImageUtils_convertYUV420SPToARGB8888(
        JNIEnv* env, jclass clazz, jbyteArray input, jintArray output,
        jint width, jint height, jboolean halfSize
) {
    jboolean inputCopy = JNI_FALSE;
    jbyte* const i = env->GetByteArrayElements(input, &inputCopy);

    jboolean outputCopy = JNI_FALSE;
    jint* const o = env->GetIntArrayElements(output, &outputCopy);

    if (halfSize) {
        ConvertYUV420SPToARGB8888HalfSize(reinterpret_cast<uint8_t*>(i),
                                          reinterpret_cast<uint32_t*>(o), width,
                                          height);
    } else {
        ConvertYUV420SPToARGB8888(reinterpret_cast<uint8_t*>(i),
                                  reinterpret_cast<uint8_t*>(i) + width * height,
                                  reinterpret_cast<uint32_t*>(o), width, height);
    }

    env->ReleaseByteArrayElements(input, i, JNI_ABORT);
    env->ReleaseIntArrayElements(output, o, 0);
}