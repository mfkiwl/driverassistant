package pl.jesmanczyk.android.driverassistant;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Environment;
import android.util.JsonReader;
import android.util.JsonWriter;

import org.tensorflow.demo.env.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

public class DetectionRepository {
    private static final Logger LOGGER = new Logger();
    private static final String IMAGES_FOLDER = "TrafficSignDetector";

    public String root;

    private List<DetectionFrame> frames;
    private List<Detection> detections;
    private String lastImageName;
    private int lastImageId;

    private int detectionCount;
    private List<String> detectionFileNames;

    public static class Detection {
        public String label;
        public RectF location;
        public Bitmap bitmap;
        public Bitmap source;
        public GpsCoords gps;
    }

    public static class DetectionFrame {
        public Bitmap bitmap;
        public List<Detection> detections;
    }

    public static class GpsCoords {
        public double latitude;
        public double longitude;
    }

    public DetectionRepository() {
        root = getDetectionsDirectoryPath();
        frames = new LinkedList<>();
        detections = new LinkedList<>();
        lastImageName = "";
        lastImageId = 1;
        detectionFileNames = new LinkedList<>();
    }

    private static String getDetectionsDirectoryPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .getAbsolutePath() + File.separator + IMAGES_FOLDER;
    }

    public void setStorageFolder(String storageFolder) {
        root = storageFolder;
    }

    public void load() {
        File folder = new File(root);
        String[] detectionFileNamesArray = folder.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                return lowercaseName.endsWith("part.jpg");
            }
        });
        detectionFileNames = new LinkedList<>(Arrays.asList(detectionFileNamesArray));
        /*
        frames.clear();
        detections.clear();
        frames = loadFrames();
        for (DetectionFrame frame : frames) {
            for (Detection detection : frame.detections) {
                detections.add(detection);
            }
        }
        */
    }

    public void addFrame(DetectionFrame frame, boolean persist) {
        if (persist) {
            String name = getCurrentImageName();
            File folder = new File(root);
            if (!folder.exists()) {
                if (!folder.mkdirs()) {
                    LOGGER.e("Failed to create folders " + root);
                }
            }
            Collection<String> parts = saveFrameImagesAndAnnotations(folder, name, frame);
            detectionFileNames.addAll(parts);
        } else {
            frames.add(frame);
            detections.addAll(frame.detections);
            cleanup();
        }
    }

    public void addFrameDetections(Collection<Detection> detections) {
        if (detections.isEmpty()) return;
        DetectionFrame detectionFrame = new DetectionFrame();
        detectionFrame.detections = new LinkedList<>(detections);
        Bitmap frame = null;
        for (Detection detection : detections) {
            frame = detection.source;
        }
        detectionFrame.bitmap = frame;
        this.detections.addAll(detections);
        frames.add(detectionFrame);
        cleanup();
    }

    public int getHistorySize() {
        return detections.size() + detectionFileNames.size();
    }

    public Detection getHistoryItem(int pos) {
        Detection detection = new Detection();
        if (pos < detections.size()) {
            int n = detections.size();
            pos = n - 1 - pos;
            detection = detections.get(pos);
        } else {
            int n = detectionFileNames.size() + detections.size();
            pos = n - 1 - pos;
            String name = detectionFileNames.get(pos);
            String path = root + File.separator + name;
            detection.bitmap = BitmapFactory.decodeFile(path);
            detection.label = name;
            loadFrameAnnotation(new File(root), name.replace(".jpg", ""), detection);
        }
        return detection;
    }

    private void cleanup() {
        final int maxSize = 8;
        while (this.detections.size() > maxSize) {
            this.detections.remove(0);
        }
        while (frames.size() > maxSize) {
            frames.remove(0);
        }
    }

    private List<DetectionFrame> loadFrames() {
        List<DetectionFrame> results = new LinkedList<>();
        int i = 0;
        File folder = new File(root);
        while (true) {
            File image = new File(folder, getFrameFileName(i) + ".jpg");
            if (!image.exists()) break;
            DetectionFrame frame = new DetectionFrame();
            frame.detections = new LinkedList<>();
            try {
                FileInputStream stream = new FileInputStream(image);
                frame.bitmap = BitmapFactory.decodeStream(stream);
                Detection detection = new Detection();
                loadFrameAnnotation(folder, getFrameFileName(i) + ".json", detection);
                try {
                    if (frame.bitmap != null) {
                        detection.bitmap = Bitmap.createBitmap(
                                frame.bitmap, (int) detection.location.left, (int) detection.location.top,
                                (int) detection.location.width(), (int) detection.location.height()
                        );
                    }
                    detection.source = frame.bitmap;
                    frame.detections.add(detection);
                } catch (Throwable t) {
                    LOGGER.e(t, "Failed to create bitmap for detection " + getFrameFileName(i));
                }

            } catch (java.io.IOException e) {
                LOGGER.e(e, "Failed to loadFrames detection " + i);
                break;
            }
            if (frame.detections.isEmpty()) break;
            results.add(frame);
            i++;
        }
        return results;
    }

    private void saveDetectionAnnotation(File folder, String name, Detection detection) {
        File json = new File(folder, name + ".json");
        if (json.exists()) json.delete();
        try (FileOutputStream out = new FileOutputStream(json)) {
            JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(out));
            jsonWriter.setIndent("    ");
            jsonWriter.beginArray();
            //for (Detection detection : frame.detections) {
                jsonWriter.beginObject();
                jsonWriter.name("label").value(detection.label);
                jsonWriter.name("left").value(detection.location.left);
                jsonWriter.name("right").value(detection.location.right);
                jsonWriter.name("top").value(detection.location.top);
                jsonWriter.name("bottom").value(detection.location.bottom);
                if (detection.gps != null) {
                    jsonWriter.name("latitude").value(detection.gps.latitude);
                    jsonWriter.name("longitude").value(detection.gps.longitude);
                }
                jsonWriter.endObject();
            //}
            jsonWriter.endArray();
            jsonWriter.close();
            out.flush();
            out.close();
        } catch (java.io.IOException e) {
            LOGGER.e(e, "Failed to save annotation " + name);
        }
    }

    private void loadFrameAnnotation(File folder, String fileName, Detection detection) {
        File json = new File(folder, fileName + ".json");
        if (!json.exists()) return;
        try {
            FileInputStream stream = new FileInputStream(json);
            JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
            reader.beginArray();
            detection.location = new RectF();
            while (reader.hasNext()) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("label")) {
                        detection.label = reader.nextString();
                    } else if (name.equals("left")) {
                        detection.location.left = (float) reader.nextDouble();
                    } else if (name.equals("top")) {
                        detection.location.top = (float) reader.nextDouble();
                    } else if (name.equals("right")) {
                        detection.location.right = (float) reader.nextDouble();
                    } else if (name.equals("bottom")) {
                        detection.location.bottom = (float) reader.nextDouble();
                    } else if (name.equals("latitude")) {
                        detection.gps = detection.gps == null ? new GpsCoords() : detection.gps;
                        detection.gps.latitude = reader.nextDouble();
                    } else if (name.equals("longitude")) {
                        detection.gps = detection.gps == null ? new GpsCoords() : detection.gps;
                        detection.gps.longitude = reader.nextDouble();
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            }
            reader.endArray();
        } catch (java.io.IOException e) {
            LOGGER.e(e, "Failed to loadFrames detection " + fileName);
        }
    }

    private void saveFrame(File folder, String name, DetectionFrame frame) {
        File image = new File(folder, name + ".jpg");
        if (image.exists()) image.delete();
        try (FileOutputStream out = new FileOutputStream(image)) {
            frame.bitmap.compress(Bitmap.CompressFormat.JPEG, 99, out);
            out.flush();
            out.close();
        } catch (java.io.IOException e) {
            LOGGER.e(e, "Failed to save detection " + name);
        }
        //saveDetectionAnnotation(folder, name, frame);
    }

    public void saveFrames(Collection<DetectionFrame> frames) {
        File folder = new File(root);
        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                throw new RuntimeException("Failed to create folders " + root);
            }
        }
        int i = 0;
        for (DetectionFrame frame : frames) {
            String name = getFrameFileName(i);
            saveFrame(folder, name, frame);
            i++;
        }
    }

    private String getFrameFileName(int i) {
        return String.format("%02d", i);
    }

    public void saveDetections(Collection<Detection> detections) {
        HashMap<Bitmap, DetectionFrame> frames = new HashMap();
        for (Detection detection : detections) {
            DetectionFrame frame;
            if (frames.containsKey(detection.source)) {
                frame = frames.get(detection.source);
            } else {
                frames.put(detection.source, frame = new DetectionFrame());
                frame.detections = new LinkedList<>();
                frame.bitmap = detection.source;
            }
            frame.detections.add(detection);
        }
        saveFrames(frames.values());
    }

    private Collection<String> saveFrameImagesAndAnnotations(File folder, String name, DetectionFrame frame) {
        saveBitmap(folder, name, frame.bitmap);
        int i = 1;
        LinkedList<String> parts = new LinkedList<>();
        for (Detection detection : frame.detections) {
            String detectionName = name + "_" +
                    detection.label.trim().replace(' ', '_') +
                    "_" + i + "_part";
            saveBitmap(folder, detectionName, detection.bitmap);
            parts.add(detectionName + ".jpg");
            saveDetectionAnnotation(folder, detectionName, detection);
            i++;
        }
        return parts;
    }

    private void saveBitmap(File folder, String name, Bitmap bitmap) {
        File image = new File(folder, name + ".jpg");
        if (image.exists()) image.delete();
        try {
            FileOutputStream out = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 99, out);
            out.flush();
            out.close();
        } catch (java.io.IOException e) {
            LOGGER.e(e, "Failed to save bitmap " + image.getAbsolutePath());
        }
    }

    private String getCurrentImageName() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        df.setTimeZone(tz);
        String now = df.format(new Date());
        if (lastImageName.equals(now)) {
            lastImageId++;
            now = now + "_" + lastImageId;
        } else {
            lastImageId = 1;
        }
        lastImageName = now;
        return now;
    }

    public static void clearHistory() {
        String root = getDetectionsDirectoryPath();
        try {
            LOGGER.i("Clear history in " + root);
            //detections.clear();
            //detectionFileNames.clear();
            clearHistory(root);
        } catch(Throwable e) {
            LOGGER.e(e, "Failed to clear history in " + root);
        }
    }

    public static void clearHistory(String path) {
        try {
            File folder = new File(path);
            File[] files = folder.listFiles();
            for (File file: files) {
                file.delete();
            }
        } catch (Throwable e) {
            throw new RuntimeException("Failed to list files in " + path, e);
        }
    }
}