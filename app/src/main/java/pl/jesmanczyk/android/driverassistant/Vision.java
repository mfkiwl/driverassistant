package pl.jesmanczyk.android.driverassistant;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Vision {
    public static interface ImageRotationAware {
        int getImageRotation();
    }

    public static class Size {
        public int width;
        public int height;

        public Size() {}

        public Size(int w, int h) {
            width = w;
            height = h;
        }

        public Size(Camera.Size s) {
            width = s.width;
            height = s.height;
        }
   }

    private static class Detections extends LinkedList<DetectionRepository.Detection> {};

    private float detectionThreshod = 0.001f;
    private ImageRotationAware imageRotationAware;
    private Size previewSize;
    private int[] rgbBytes;
    private Bitmap rgbFrameBitmap;
    private Detector detector;
    private Detector.Crop cropper;
    private Classifier refiner;
    private AtomicInteger curId;
    private Set<String> skipLabels;
    private AssetManager assets;

    public Vision(AssetManager assetManager, ImageRotationAware aware) {
        detector = new Detector(assets = assetManager);
        cropper = new Detector.Crop(detector.getInputSize());
        curId = new AtomicInteger(0);
        imageRotationAware = aware;
        skipLabels = new HashSet<>();
    }

    public void reset() {
        rgbBytes = null;
        rgbFrameBitmap = null;
    }

    public void imageSizeChanged(Size s) {
        previewSize = s;
        try {
            rgbBytes = new int[previewSize.width * previewSize.height];
            rgbFrameBitmap = Bitmap.createBitmap(previewSize.width, previewSize.height, Bitmap.Config.ARGB_8888);
        } catch(Throwable e) {
            reset();
            throw e;
        }
    }

    public Vision setRefine(boolean r) {
        if (r) {
            if (refiner == null) {
                refiner = new Classifier(assets);
            }
        } else {
            refiner = null;
        }
        return this;
    }

    public Vision setDetectionThreshold(float threshold) {
        detectionThreshod = threshold;
        return this;
    }

    public Vision setSkipLabels(Set<String> labels) {
        skipLabels = labels;
        return this;
    }

    public DetectionRepository.DetectionFrame look(byte[] bytes) {
        ImageUtils.convertYUV420SPToARGB8888(bytes, previewSize.width, previewSize.height, rgbBytes);
        rgbFrameBitmap.setPixels(rgbBytes, 0, previewSize.width, 0, 0, previewSize.width, previewSize.height);
        Matrix rotationMatrix = new Matrix();
        rotationMatrix.postRotate(imageRotationAware.getImageRotation());
        DetectionRepository.DetectionFrame frame = new DetectionRepository.DetectionFrame();
        frame.bitmap = Bitmap.createBitmap(rgbFrameBitmap, 0, 0,
                rgbFrameBitmap.getWidth(), rgbFrameBitmap.getHeight(),
                rotationMatrix, true);
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.postScale(
                frame.bitmap.getWidth() * 1.0f / detector.getInputSize(),
                frame.bitmap.getHeight() * 1.0f / detector.getInputSize()
        );
        Bitmap croppedBitmap = cropper.apply(frame.bitmap);
        List<Detector.Recognition> recognitions = detector.recognize(croppedBitmap);
        frame.detections = new Detections();
        for(Detector.Recognition recognition : recognitions) {
            if (recognition.getConfidence() > detectionThreshod) {
                RectF location = recognition.getLocation();
                if (location != null) {
                    scaleMatrix.mapRect(location);
                    final Bitmap cut = Bitmap.createBitmap(frame.bitmap,
                            (int) location.left, (int) location.top,
                            (int) location.width(), (int) location.height());
                    String label = "";
                    if (refiner != null) {
                        if (refiner != null && curId.getAndIncrement() % 1 == 0) {
                            label = refiner.classify(cut);
                        }
                    }
                    if (!skipLabels.contains(label)) {
                        DetectionRepository.Detection d = new DetectionRepository.Detection();
                        d.label = label.isEmpty() ? recognition.getTitle() : label;
                        d.location = location;
                        d.source = frame.bitmap;
                        d.bitmap = cut;
                        frame.detections.add(d);
                    }
                }
            }
        }
        return frame;
    }
}
