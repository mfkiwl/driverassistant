package pl.jesmanczyk.android.driverassistant;

import android.hardware.Camera;
import android.util.Size;

import java.util.ArrayList;
import java.util.List;

public class CameraParametersAdapter {
    public static class Size {
        int width;
        int height;

        public Size(int w, int h) {
            width = w;
            height = h;
        }

        public static Size fromCameraSize(Camera.Size size) {
            Size s = new Size(size.width, size.height);
            return s;
        }
    }

    private Camera.Parameters source;
    private Size pictureSize;
    private Size previewSize;
    public List<Size> supportedPictureSizes;
    public List<Size> supportedPreviewSizes;

    {
        supportedPictureSizes = new ArrayList<>();
        supportedPreviewSizes = new ArrayList<>();
    }

    public CameraParametersAdapter() {
    }

    public CameraParametersAdapter(CameraParametersAdapter parameters) {
        source = parameters.source;
    }

    public static CameraParametersAdapter fromCameraParameters(Camera.Parameters params) {
        CameraParametersAdapter adapter = new CameraParametersAdapter();
        adapter.source = params;
        return adapter;
    }

    public static CameraParametersAdapter calcImageSize(CameraParametersAdapter parameters, int orientation, int minCameraFrameHeight) {
        List<Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
        List<Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        if (supportedPictureSizes.size() < 1) return parameters;
        parameters = new CameraParametersAdapter(parameters);
        Size pictureSize = calcSize(supportedPictureSizes, orientation, minCameraFrameHeight);
        parameters.setPictureSize(pictureSize.width, pictureSize.height);
        if (supportedPictureSizes.size() < 1) return parameters;
        Size previewSize = calcSize(supportedPreviewSizes, orientation, minCameraFrameHeight);
        parameters.setPreviewSize(previewSize.width, previewSize.height);
        return parameters;
    }

    public Size getPictureSize() {
        if (source == null) return pictureSize;
        return Size.fromCameraSize(source.getPictureSize());
    }

    public Size getPreviewSize() {
        if (source == null) return previewSize;
        return Size.fromCameraSize(source.getPreviewSize());
    }

    private static Size calcSize(List<Size> sizes, int orientation, int minCameraFrameHeight) {
        Size min = null;
        Size minRaw = null;
        boolean swapDimensions = orientation == 90 || orientation == 270;
        for (Size size : sizes) {
            if (dimensionsFit(size, swapDimensions, minCameraFrameHeight) && isMinSize(min, size)) min = size;
            if (dimensionsFit(size, false, minCameraFrameHeight) && isMinSize(minRaw, size)) minRaw = size;
        }
        if (min == null) min = minRaw;
        if (min == null) min = sizes.get(0);
        return new Size(min.width, min.height);

    }

    private static boolean dimensionsFit(Size size, boolean vertical, int minCameraFrameHeight) {
        int width = vertical ? size.height : size.width;
        int height = vertical ? size.width : size.height;
        float ratio = (float)height / width;
        boolean ratioFits = 0.74 < ratio && ratio < 0.76;
        return ratioFits && height >= minCameraFrameHeight;
    }

    private static int area(Size size) { return size.width * size.height; }

    private static boolean isMinSize(Size min, Size size) {
        return min == null || area(size) < area(min);
    }

    private void setPreviewSize(int width, int height) {
        if (source == null) {
            previewSize = new Size(width, height);
        } else {
            source.setPreviewSize(width, height);
        }
    }

    private void setPictureSize(int width, int height) {
        if (source == null) {
            pictureSize = new Size(width, height);
        } else {
            source.setPictureSize(width, height);
        }
    }

    private List<Size> getSupportedPictureSizes() {
        if (source == null) return supportedPictureSizes;
        List<Size> result = new ArrayList<>();
        for(Camera.Size s: source.getSupportedPictureSizes()) {
            result.add(Size.fromCameraSize(s));
        }
        return result;
    }

    private List<Size> getSupportedPreviewSizes() {
        if (source == null) return supportedPreviewSizes;
        List<Size> result = new ArrayList<>();
        for(Camera.Size s: source.getSupportedPreviewSizes()) {
            result.add(Size.fromCameraSize(s));
        }
        return result;
    }

}
